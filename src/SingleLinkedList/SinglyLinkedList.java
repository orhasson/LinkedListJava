package SingleLinkedList;

import Node.Node;

public class SinglyLinkedList<T> implements Cloneable,Comparable<T> {
    private Node<T> head;
    private Node<T> tail;
    private int sizeOfTheList;


    public SinglyLinkedList(T data){
        this.head = new Node<>(data);
        this.head.setNext(null);
        this.tail = this.head;
        this.sizeOfTheList = 1;
    }

    public Node<T> getHeadOfTheList(){
        return this.head;
    }

    public Node<T> getTailOfTheList(){
        return this.tail;
    }

    public void setHeadOfTheList(Node<T> head){
        this.head = head;
    }

    public void setTailOfTheList(Node<T> tail){
        this.head = tail;
    }

    public int getSizeOfTheList(){
        return this.sizeOfTheList;
    }

    private boolean isListExist(){
        return this.head != null;
    }

    public void insertElementToTheList(T data, int locationIndex){
        Node<T> node = new Node<>(data);

        //The Linked List doesn't exist
        if(!isListExist()){
            System.out.println("The Linked List doesn't exist");
        }

        //insert the New Node.Node at first position
        else if(locationIndex == 0){
            node.setNext(head);
            this.head = node;
        }

        //insert the New Node.Node at the last position
        else if(locationIndex >= this.sizeOfTheList){
            node.setNext(null);
            this.tail.setNext(node);
            this.tail = node;
        }

        else{
            Node<T> tempNode = this.head;
            int index = 0;
            while(index < locationIndex - 1){
                tempNode = tempNode.getNext();
                index++;
            }

            Node<T> nextNode = tempNode.getNext();
            tempNode.setNext(node);
            node.setNext(nextNode);
        }
        this.sizeOfTheList ++;
    }

    //Print All The List
    public void printTheLinkedList(){
        if(isListExist()){
            Node<T> tempNode = head;
            for(int i = 0; i < this.sizeOfTheList; i++){
                System.out.print(tempNode.getData().toString());
                if(i != this.sizeOfTheList - 1 ){
                    System.out.print(" -> ");
                }
                tempNode = tempNode.getNext();
            }
        }
        else{
            System.out.println("The Linked List doesn't exist");
        }
        System.out.println("\n");
    }

    //Delete all the list;
    public void deleteLinkidList(){
        head = null;
        tail = null;
        System.out.println("Linked List Deleted!");
    }


    public boolean searchForNodeWithData(T data){
        if(isListExist()){
            Node<T> tempNode = head;
            for(int i = 0; i < this.sizeOfTheList; i++){
                if(tempNode.getData().equals(data)){
                    System.out.println("Found the value " + data.toString() + " in location " + i);
                    return true;
                }
                tempNode = tempNode.getNext();

            }
        }
        else{
            System.out.println("The Linked List doesn't exist");
        }
        System.out.println("data not Found in LinkedList...");
        return false;
    }

    public boolean deleteSpecificNodeWithDataFromTheList(T data){
        if(!isListExist()){
            System.out.println("The Linked List doesn't exist");
            return false;
        }

        else if(head.getData().equals(data)){
            head = head.getNext();
            this.sizeOfTheList --;
            return true;
        }

        else{
            Node<T> tempNode = head;
            for(int i = 0; i <= this.sizeOfTheList; i++){
                if((tempNode.getNext().getData()).equals(data)){
                    tempNode.setNext(tempNode.getNext().getNext());
                    this.sizeOfTheList --;
                    return true;
                }
                tempNode = tempNode.getNext();
            }
        }
        return false;
    }




    @Override
    public int compareTo(T o) {
        return 0;
    }
}
