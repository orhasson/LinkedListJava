package Node;

public class Node<T> implements Comparable<T> {

    private T data;
    private Node<T> next;

    /**
     * Constructor Node.Node contains
     * //the Node.Node's data and set the
     * next reference to null
     **/
    public Node(T data) {
        this.data = data;
        setNext(null);
    }

    //Return the next Node.Node Element in the list
    public Node<T> getNext() {
        return this.next;
    }

    //Setting the next Node.Node element in the list
    public void setNext(Node<T> next) {
        this.next = next;
    }

    //Getting the T data of Element;
    public T getData(){
        return this.data;
    }




    //The String Representation of the Node.Node is The String
    //Representation of the DATA that storing in the Node.Node
    @Override
    public String toString(){
        return String.valueOf(this.data);
    }


    public boolean equalsValue(Node<T> node,T data){
        return node.getData() == data;
    }

    @Override
    public int compareTo(T o) {
        return 0;
    }
}