package SingleCircularLinkedList;

public class MainTestSingleCircularLinkedList {


    public static void main(String[] args) {

        SingleCircularLinkedList list = new SingleCircularLinkedList<>(5);
        list.printTheLinkedList();

        list.insertElementToTheList(10, 1);
        list.printTheLinkedList();

        list.insertElementToTheList(20, 2);
        list.printTheLinkedList();

        list.insertElementToTheList(30, 2);
        list.printTheLinkedList();

        list.insertElementToTheList(40, 1);
        list.printTheLinkedList();

        System.out.println();


        System.out.println("\nSearching the node having value 40...");
        list.searchForNodeWithData(40);

        System.out.println("\nSearching the node having value 500...");
        list.searchForNodeWithData(500);



        System.out.println("Before Deletion:");
        list.printTheLinkedList();
        list.deleteSpecificNodeWithDataFromTheList(5);
        System.out.println("After Deletion:");
        list.printTheLinkedList();
        System.out.println();


        System.out.println("Before Deletion:");
        list.printTheLinkedList();
        list.deleteSpecificNodeWithDataFromTheList(20);
        System.out.println("After Deletion:");
        list.printTheLinkedList();
        System.out.println();



        System.out.println("Before Deletion:");
        list.printTheLinkedList();
        list.deleteSpecificNodeWithDataFromTheList(10);
        System.out.println("After Deletion:");
        list.printTheLinkedList();
        System.out.println();


        list.deleteLinkidList();
        list.printTheLinkedList();


    }

}
